//
//  ServicesViewController.swift
//  SmartCityApp
//
//  Created by Vladimir Gnatiuk on 7/5/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit


let services = [
	"Medicine",
	"Mobile reception",
	"The police",
	"Public services",
	"Road Service",
	"Firefighters and emergency services",
	"Weather",
	"Electronic accounting system",
	"Transport",
	"Energy"]

class ServicesViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
	
	@IBOutlet weak var tableView: UITableView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		titleLabel.text = "Services"
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return services.count
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("ServiceCell", forIndexPath: indexPath) as! ServiceCell
		cell.serviceLabel?.text = services[indexPath.row]
		cell.backgroundColor = UIColor.clearColor()
		return cell
	}
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		if indexPath.row == 0 {
			let storyboard = UIStoryboard(name: "Main", bundle: nil)
			var cityViewController = storyboard.instantiateViewControllerWithIdentifier("EventsViewController") as! EventsViewController
			self.navigationController?.pushViewController(cityViewController, animated: true)
			tableView.deselectRowAtIndexPath(indexPath, animated: false)
		}
//		if let user = User.first() as? User {
//			Alamofire.request(.POST, "http://104.131.75.176/api/facebook/login", parameters: ["access_token": user.token]).responseJSON { (_, _, jsonObject, _) in
//				let json = SwiftyJSON.JSON(jsonObject!)
//				if let userId = json["id"].number {
//					user.userId = userId
//					user.city = cities[indexPath.row]
//					AERecord.saveContextAndWait()
//				}
//			}
//			
//		}
	}
}

class ServiceCell: UITableViewCell {
	
	@IBOutlet weak var serviceLabel: UILabel!
}