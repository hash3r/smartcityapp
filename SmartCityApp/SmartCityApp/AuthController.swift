//
//  AuthViewController.swift
//  MVP
//
//  Created by Vladimir Gnatiuk on 6/30/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit
import Foundation
import FBSDKLoginKit
import FBSDKCoreKit
import Async
import AERecord

public typealias AuthViewControllerSuccessBlock = () -> ()
public typealias AuthViewControllerFailureBlock = (NSError?) -> ()

class AuthViewController: UIViewController {
	
	var successBlock: AuthViewControllerSuccessBlock!
	var failureBlock: AuthViewControllerFailureBlock!
	
	@IBOutlet weak var nameField: UITextField!
	@IBOutlet weak var emailField: UITextField!
	@IBOutlet weak var passField: UITextField!
	
	@IBOutlet var textFieldPlaceholder: [UIView]!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		var titleLabel = UILabel(frame: CGRectMake(0, 0, 60, 40))
		titleLabel.textAlignment = NSTextAlignment.Center
		titleLabel.textColor = UIColor.whiteColor()
		titleLabel.text = "MVP"
		navigationItem.titleView = titleLabel

		successBlock = {
			let storyboard = UIStoryboard(name: "Main", bundle: nil)
			var vc = storyboard.instantiateViewControllerWithIdentifier("ServicesViewController") as! ServicesViewController
			var navController = NavigationController(rootViewController: vc)
			self.showViewController(navController, sender: self)
//			self.showViewController(TabBarController(), sender: self)
		}
		failureBlock = {error in
			var alert = UIAlertController(title: "Can not login", message: error?.description, preferredStyle: UIAlertControllerStyle.Alert)
			//				alert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { action in
			//				}))
			alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
			}))
			let delay = 0.5 * Double(NSEC_PER_SEC)
			//				let popTime = dispatch_time(DISPATCH_TIME_NOW, Int64(delay));
			Async.main(after: delay) {
				self.presentViewController(alert, animated: true, completion: nil)
			}
		}
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	@IBAction func enterWithFacebook(sender: UIButton) {
		
		loginToFacebookWithSuccess(successBlock, failureBlock: failureBlock)
	}
	
	let facebookReadPermissions = ["public_profile", "email", "user_hometown"]
	//Some other options: "user_about_me", "user_birthday", "user_hometown", "user_likes", "user_interests", "user_photos", "friends_photos", "friends_hometown", "friends_location", "friends_education_history"
 
	func loginToFacebookWithSuccess(successBlock: () -> (), failureBlock: (NSError?) -> ()) {
		
		if FBSDKAccessToken.currentAccessToken() != nil {
			//For debugging, when we want to ensure that facebook login always happens
			//FBSDKLoginManager().logOut()
			//Otherwise do:
			return
		}
		
		FBSDKLoginManager().logInWithReadPermissions(self.facebookReadPermissions, handler: { (result:FBSDKLoginManagerLoginResult!, error:NSError!) -> Void in
			if error != nil {
				//According to Facebook:
				//Errors will rarely occur in the typical login flow because the login dialog
				//presented by Facebook via single sign on will guide the users to resolve any errors.
				
				// Process error
				FBSDKLoginManager().logOut()
				failureBlock(error)
			} else if result.isCancelled {
				// Handle cancellations
				FBSDKLoginManager().logOut()
				failureBlock(nil)
			} else {
				// If you ask for multiple permissions at once, you
				// should check if specific permissions missing
				var allPermsGranted = true
				
				//result.grantedPermissions returns an array of _NSCFString pointers
				let grantedPermissions = Array(result.grantedPermissions).map( {"\($0)"} )
				for permission in self.facebookReadPermissions {
					if !contains(grantedPermissions, permission) {
						allPermsGranted = false
						break
					}
				}
				if allPermsGranted {
					// Do work
					let fbToken = result.token.tokenString
					let fbUserID = result.token.userID
					
					if let user = User.first() as? User {
						user.token = fbToken
					} else {
						User.createWithAttributes(["token" : fbToken])
					}
					AERecord.saveContextAndWait()

					let userRequest = FBSDKGraphRequest(graphPath:"/\(fbUserID)", parameters: nil, HTTPMethod:"GET")
					userRequest.startWithCompletionHandler({
						(connection, result, error: NSError!) -> Void in
						if error == nil {
							println("\(result)")
							if let address = result["address"] as? String  {
								println("\(address)")
							} else {
								let storyboard = UIStoryboard(name: "Main", bundle: nil)
								var cityViewController = storyboard.instantiateViewControllerWithIdentifier("CityViewController") as! CityViewController
								self.navigationController?.pushViewController(cityViewController, animated: true)
							}
						} else {
							println("\(error)")
						}
					})
					
					//Send fbToken and fbUserID to your web API for processing, or just hang on to that locally if needed
					//self.post("myserver/myendpoint", parameters: ["token": fbToken, "userID": fbUserId]) {(error: NSError?) ->() in
					//	if error != nil {
					//		failureBlock(error)
					//	} else {
					//		successBlock(maybeSomeInfoHere?)
					//	}
					//}
	
//					!!!!!!!!!
//					successBlock()
				} else {
					//The user did not grant all permissions requested
					//Discover which permissions are granted
					//and if you can live without the declined ones
					
					failureBlock(nil)
				}
			}
		})
	}

}