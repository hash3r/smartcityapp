//
//  PusherManager.swift
//  SmartCityApp
//
//  Created by Vladimir Gnatiuk on 7/5/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import Foundation
import Pusher

public class PusherManager : NSObject, PTPusherDelegate {
	
	public class var sharedConfig: PusherManager {
		struct Static {
			static let instance : PusherManager = PusherManager()
		}
		return Static.instance
	}

	public var client = PTPusher()
	
	public override init() {
		super.init()

		if let user = User.first() as? User {

//			PUSHER_APP_ID = '92581'
//			PUSHER_KEY = 'f27b2c833440547c21c7'
//			PUSHER_SECRET = ‘5a8290906ddff6c79da7'
//			
			client = PTPusher.pusherWithKey("f27b2c833440547c21c7", delegate:self, encrypted: false) as! PTPusher
			client.authorizationURL = NSURL(string: "http://f27b2c833440547c21c7:5a8290906ddff6c79da7@api.pusher.com/apps/92581")
			client.connect()

	//		var presenceDelegate: PTPusherPresenceChannelDelegate?
//			let channelName = "notifications"//_\(user.bloodType)_\(user.resus)"
//			
//			var channel = client.subscribeToChannelNamed(channelName) as PTPusherChannel
//
//			channel.bindToEventNamed("new_notification", handleWithBlock: { channelEvent in
//				var message = channelEvent.data.objectForKey("id") as! String
//				println(message)
//			})
		}
	}
	
	public func pusher(pusher: PTPusher!, connectionDidConnect connection: PTPusherConnection!) {
		println("cool")
	}
	
	public func pusher(pusher: PTPusher!, didSubscribeToChannel channel: PTPusherChannel!) {
		println("cool")
	}
	
	public func pusher(pusher: PTPusher!, didFailToSubscribeToChannel channel: PTPusherChannel!, withError error: NSError!) {
		println("cool")
	}
	
	public func pusher(pusher: PTPusher!, didReceiveErrorEvent errorEvent: PTPusherErrorEvent!) {
		println("cool")
	}



	
	public func pusher(pusher: PTPusher!, connection: PTPusherConnection!, failedWithError error: NSError!) {
		println(error)
	}
	
//	public func presenceChannelDidSubscribe(channel: PTPusherPresenceChannel!) {
//		
//	}

}