//
//  CityViewController.swift
//  SmartCityApp
//
//  Created by Vladimir Gnatiuk on 7/3/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AERecord

let cities = ["Киев",
	"Одесса",
	"Харьков",
	"Днепропетровск",
	"Запорожье",
	"Винница",
	"Николаев",
	"Львов",
	"Ужгород",
	"Херсон"]

class CityViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

	override func viewDidLoad() {
		super.viewDidLoad()
		titleLabel.text = "Select city"
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return cities.count
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("cityCell", forIndexPath: indexPath) as! CityCell
		cell.cityLabel?.text = cities[indexPath.row]
//		UIView *backView = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
		cell.backgroundColor = UIColor.clearColor()
//		cell.backgroundView = backView;
		return cell
	}
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//		TODO: save city
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		var cityViewController = storyboard.instantiateViewControllerWithIdentifier("BloodTypeController") as! BloodTypeController
		self.navigationController?.pushViewController(cityViewController, animated: true)

		if let user = User.first() as? User {
			Alamofire.request(.POST, "http://104.131.75.176/api/facebook/login", parameters: ["access_token": user.token]).responseJSON { (_, _, jsonObject, _) in
				let json = SwiftyJSON.JSON(jsonObject!)
				if let userId = json["id"].number {
					user.userId = userId
					user.city = cities[indexPath.row]
					AERecord.saveContextAndWait()
				}
			}

		}
	}
}

class CityCell: UITableViewCell {
	
	@IBOutlet weak var cityLabel: UILabel!
}
