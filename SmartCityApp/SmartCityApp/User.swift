//
//  User.swift
//  SmartCityApp
//
//  Created by Vladimir Gnatiuk on 7/5/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import Foundation
import CoreData

class User: NSManagedObject {

    @NSManaged var city: String
    @NSManaged var email: String
    @NSManaged var name: String
    @NSManaged var token: String
    @NSManaged var userId: NSNumber
    @NSManaged var bloodType: String
    @NSManaged var resus: String

}
