//
//  AppDelegate.swift
//  SmartCityApp
//
//  Created by Vladimir Gnatiuk on 7/3/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit
import CoreData
import FBSDKLoginKit
import FBSDKCoreKit
import AERecord

let authAlwaysOn = true

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

	func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
		// Override point for customization after application launch.
		AERecord.loadCoreDataStack()

		window = UIWindow(frame:UIScreen.mainScreen().bounds)
		//		fontList()
		setupAppearance()
		pushRegistration()
		
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		if authAlwaysOn == false {
			if let user = User.first() as? User {
				let storyboard = UIStoryboard(name: "Main", bundle: nil)
				var vc = storyboard.instantiateViewControllerWithIdentifier("ServicesViewController") as! ServicesViewController
				var navController = NavigationController(rootViewController: vc)
				window?.rootViewController = navController
				let pusher = PusherManager.sharedConfig
			} else {
				var authNavController = storyboard.instantiateViewControllerWithIdentifier("AuthNavController") as! UINavigationController
				window?.rootViewController = authNavController
			}
		} else {
			var authNavController = storyboard.instantiateViewControllerWithIdentifier("AuthNavController") as! UINavigationController
			window?.rootViewController = authNavController
		}
	
//		let completeAction = UIMutableUserNotificationAction()
//		completeAction.identifier = "COMPLETE_TODO" // the unique identifier for this action
//		completeAction.title = "Complete" // title for the action button
//		completeAction.activationMode = .Background // UIUserNotificationActivationMode.Background - don't bring app to foreground
//		completeAction.authenticationRequired = false // don't require unlocking before performing action
//		completeAction.destructive = true // display action in red
//		
//		let remindAction = UIMutableUserNotificationAction()
//		remindAction.identifier = "REMIND"
//		remindAction.title = "Remind in 30 minutes"
//		remindAction.activationMode = .Background
//		remindAction.destructive = false
//		
//		let todoCategory = UIMutableUserNotificationCategory() // notification categories allow us to create groups of actions that we can associate with a notification
//		todoCategory.identifier = "TODO_CATEGORY"
//		todoCategory.setActions([remindAction, completeAction], forContext: .Default) // UIUserNotificationActionContext.Default (4 actions max)
//		todoCategory.setActions([completeAction, remindAction], forContext: .Minimal) // UIUserNotificationActionContext.Minimal - for when space is limited (2 actions max)
//		
//		application.registerUserNotificationSettings(UIUserNotificationSettings(forTypes: .Alert | .Badge | .Sound, categories: NSSet(array: [todoCategory]) as Set<NSObject>)) // we're now providing a set containing our category as an argument
	
		window?.makeKeyAndVisible()
		return true
	}


	func setupAppearance() {
		//		let back_button = UIImage(named: "nack_button")?.resizableImageWithCapInsets(UIEdgeInsetsMake(0, 25, 0, 0))
		//		UIBarButtonItem.appearance().setBackButtonBackgroundImage(back_button, forState: UIControlState.Normal, barMetrics: UIBarMetrics.Default)
		UINavigationBar.appearance().barTintColor = UIColor(0xff3b00)
		UINavigationBar.appearance().tintColor = UIColor.whiteColor()
		UITabBar.appearance().tintColor = UIColor(0xff3b00)
		UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false)
	}

	func pushRegistration() {
		var type = UIUserNotificationType.Badge | UIUserNotificationType.Alert | UIUserNotificationType.Sound;
		var setting = UIUserNotificationSettings(forTypes: type, categories: nil);
		UIApplication.sharedApplication().registerUserNotificationSettings(setting);
		UIApplication.sharedApplication().registerForRemoteNotifications();
	}
	
	func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
		
		println("Recived: \(userInfo)")
		//Parsing userinfo:
		var temp : NSDictionary = userInfo
		if let info = userInfo["aps"] as? Dictionary<String, AnyObject>
		{
			var alertMsg = info["alert"] as! String
			var alert: UIAlertView!
			alert = UIAlertView(title: "", message: alertMsg, delegate: nil, cancelButtonTitle: "OK")
			alert.show()
		}
	}
	
	func application(application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
		println("didRegisterForRemoteNotificationsWithDeviceToken")
		var characterSet: NSCharacterSet = NSCharacterSet( charactersInString: "<>" )
		
		var deviceTokenString: String = ( deviceToken.description as NSString )
			.stringByTrimmingCharactersInSet( characterSet )
			.stringByReplacingOccurrencesOfString( " ", withString: "" ) as String
		
		println( deviceTokenString )
		//send this device token to server
	}
	
	//Called if unable to register for APNS.
	func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
		println(error)
	}
	
	func applicationWillResignActive(application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
		var notification = UILocalNotification()
		notification.alertBody = "You can save a lot of lives!" // text that will be displayed in the notification
		notification.alertAction = "open" // text that is displayed after "slide to..." on the lock screen - defaults to "slide to view"
		notification.fireDate = NSDate(timeIntervalSinceNow: 3) // todo item due date (when notification will be fired)
		notification.soundName = UILocalNotificationDefaultSoundName // play default sound
		//			notification.userInfo = ["UUID": item.UUID, ] // assign a unique identifier to the notification so that we can retrieve it later
		notification.category = "TODO_CATEGORY"
		UIApplication.sharedApplication().scheduleLocalNotification(notification)
	}

	func applicationDidEnterBackground(application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.


	}

	func applicationWillEnterForeground(application: UIApplication) {
		// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
	}

	func applicationDidBecomeActive(application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	}

	func applicationWillTerminate(application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
		// Saves changes in the application's managed object context before the application terminates.
//		self.saveContext()
	}
	
	func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {
		//Even though the Facebook SDK can make this determinitaion on its own,
		//let's make sure that the facebook SDK only sees urls intended for it,
		//facebook has enough info already!
		let isFacebookURL = url.scheme != nil && url.scheme!.hasPrefix("fb\(FBSDKSettings.appID())") && url.host == "authorize"
		if isFacebookURL {
			return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
		}
		return false
	}

//	func setupCoreData {
//		let myModel: NSManagedObjectModel = ...
//		let myStoreType = NSInMemoryStoreType
//		let myConfiguration = ...
//		let myStoreURL = AERecord.storeURLForName("SmartCityApp")
//		let myOptions = [NSMigratePersistentStoresAutomaticallyOption : true]
//		AERecord.loadCoreDataStack(managedObjectModel: myModel, storeType: myStoreType, configuration: myConfiguration, storeURL: myStoreURL, options: myOptions)
//	}
	
	
	
	// MARK: - Core Data stack

//	lazy var applicationDocumentsDirectory: NSURL = {
//	    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.garage48.SmartCityApp" in the application's documents Application Support directory.
//	    let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
//	    return urls[urls.count-1] as! NSURL
//	}()
//
//	lazy var managedObjectModel: NSManagedObjectModel = {
//	    // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
//	    let modelURL = NSBundle.mainBundle().URLForResource("SmartCityApp", withExtension: "momd")!
//	    return NSManagedObjectModel(contentsOfURL: modelURL)!
//	}()
//
//	lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
//	    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
//	    // Create the coordinator and store
//	    var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
//	    let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SmartCityApp.sqlite")
//	    var error: NSError? = nil
//	    var failureReason = "There was an error creating or loading the application's saved data."
//	    if coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil, error: &error) == nil {
//	        coordinator = nil
//	        // Report any error we got.
//	        var dict = [String: AnyObject]()
//	        dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
//	        dict[NSLocalizedFailureReasonErrorKey] = failureReason
//	        dict[NSUnderlyingErrorKey] = error
//	        error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
//	        // Replace this with code to handle the error appropriately.
//	        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//	        NSLog("Unresolved error \(error), \(error!.userInfo)")
//	        abort()
//	    }
//	    
//	    return coordinator
//	}()
//
//	lazy var managedObjectContext: NSManagedObjectContext? = {
//	    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
//	    let coordinator = self.persistentStoreCoordinator
//	    if coordinator == nil {
//	        return nil
//	    }
//	    var managedObjectContext = NSManagedObjectContext()
//	    managedObjectContext.persistentStoreCoordinator = coordinator
//	    return managedObjectContext
//	}()
//
//	// MARK: - Core Data Saving support
//
//	func saveContext () {
//	    if let moc = self.managedObjectContext {
//	        var error: NSError? = nil
//	        if moc.hasChanges && !moc.save(&error) {
//	            // Replace this implementation with code to handle the error appropriately.
//	            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//	            NSLog("Unresolved error \(error), \(error!.userInfo)")
//	            abort()
//	        }
//	    }
//	}

}

