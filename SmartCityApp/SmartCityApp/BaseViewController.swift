//
//  BaseViewController.swift
//  MVP
//
//  Created by Vladimir Gnatiuk on 6/30/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

	var titleLabel: UILabel!
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
//		navigationController?.navigationBarHidden = true
		navigationController?.navigationBar.tintColor = UIColor.whiteColor()
//		navigationController?.navigationBar.translucent = false
		
		titleLabel = UILabel(frame: CGRectMake(0, 0, 120, 40))
		titleLabel.textAlignment = NSTextAlignment.Center
		titleLabel.font = UIFont(name: "HelveticaNeue-Light", size: 15)
		titleLabel.textColor = UIColor.whiteColor()
		navigationItem.titleView = titleLabel

//		var backButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: nil, action: nil)
//		navigationItem.backBarButtonItem = backButton
	}
}
