//
//  BloodTypeController.swift
//  SmartCityApp
//
//  Created by Vladimir Gnatiuk on 7/4/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AERecord

class BloodTypeController : BaseViewController {
	@IBOutlet weak var bloodType: UISegmentedControl!
	@IBOutlet weak var rhesus: UISegmentedControl!

	@IBAction func nextTapped(sender: AnyObject) {
		
		let bloodTypeStr = "\(bloodType.selectedSegmentIndex + 1)"
		let rhesusStr = rhesus.selectedSegmentIndex == 0 ? "true" : "false"
		if let user = User.first() as? User {
			user.resus = rhesusStr
			user.bloodType = bloodTypeStr
			AERecord.saveContextAndWait()
			PusherManager()
			var params = [
				"city": user.city,
				"blood_type": bloodTypeStr,
				"resus": rhesusStr,
				"user_id": user.userId]
			Alamofire.request(.POST, "http://104.131.75.176/api/user_profile", parameters: params)
				.responseJSON { (request, response, jsonObject, error) in
					if let jsonObject: AnyObject = jsonObject {
						let json = SwiftyJSON.JSON(jsonObject)
//						if let recepients = json["value"].array {
						self.navigationController?.popToRootViewControllerAnimated(false)
						let storyboard = UIStoryboard(name: "Main", bundle: nil)
						var vc = storyboard.instantiateViewControllerWithIdentifier("ServicesViewController") as! ServicesViewController
						var navController = NavigationController(rootViewController: vc)
						self.showViewController(navController, sender: self)
//						self.showViewController(TabBarController(), sender: self)
//					}
				}
			}
		}
	}
}