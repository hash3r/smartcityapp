//
//  EventDetailsViewController.swift
//  SmartCityApp
//
//  Created by Vladimir Gnatiuk on 7/5/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit
import SwiftyJSON
import Haneke

class EventDetailsViewController: BaseViewController {
	
	@IBOutlet weak var textView: UITextView!
	@IBOutlet weak var imageView: UIImageView!
	var data = Array<SwiftyJSON.JSON>()
//	
//	var eventsData = Array<SwiftyJSON.JSON>()
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		titleLabel.text = "Details"
		
		//		let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
		//		collectionLayout.sectionInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
		//		collectionLayout.itemSize = CGSize(width: 90, height: 90)
//		fetchEventsData()
		
		let event = data[0]
		let url = NSURL(string: event["image"].string!)
		imageView.hnk_setImageFromURL(url!)
		textView.text = data[0]["message"].string! //+ event["lastName"].string!
	}
	
}
