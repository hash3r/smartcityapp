//
//  EventsViewController.swift
//  SmartCityApp
//
//  Created by Vladimir Gnatiuk on 7/4/15.
//  Copyright (c) 2015 Vladimir Gnatiuk. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import Haneke
import Pusher
import SFSwiftNotification

var start_id = 147
var viewDidAppearCounter = 0

class EventsViewController: BaseViewController {
	
	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var collectionLayout: UICollectionViewFlowLayout!
	
	var eventsData = Array<SwiftyJSON.JSON>()
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
	}

//	var notifyView: SFSwiftNotification? = nil
//
	override func viewDidLoad() {
		super.viewDidLoad()
		titleLabel.text = "Recipients"
		
		fetchEventsData()
		
		NSNotificationCenter.defaultCenter().addObserver(self, selector: "becomeActive", name:UIApplicationDidBecomeActiveNotification, object: nil)
		
		let channelName = "notifications"//_\(user.bloodType)_\(user.resus)"
		var channel = PusherManager.sharedConfig.client.subscribeToChannelNamed(channelName) as PTPusherChannel
		
		channel.bindToEventNamed("new_notification", handleWithBlock: { channelEvent in
			var message = channelEvent.data.objectForKey("id") as! String
			println(message)
		})
	}
	
	func becomeActive() {
			let params = ["notification_id": "\(start_id)"]
			Alamofire.request(Method.GET, "http://104.131.75.176/api/notification", parameters: params)
				.responseJSON { (request, response, jsonObject, error) in
					if let jsonObject: AnyObject = jsonObject {
						let json = SwiftyJSON.JSON(jsonObject)
						//						if let recepients = json.array {
						self.eventsData.insert(json, atIndex: 0)
						let index = NSIndexPath(forItem: 0, inSection: 0)
						self.collectionView.insertItemsAtIndexPaths([index])
					}
			}
		viewDidAppearCounter++
		start_id++
	}
	
	override func viewDidAppear(animated: Bool) {
		super.viewDidAppear(animated)

	}
	
	func fetchEventsData() {
	
		let user = User.first() as? User

		var params = Dictionary<String, String>()
		if let user = user {
			params = ["user_id" : "\(user.userId.integerValue)"]
		}

//		Alamofire.request(.GET, "http://104.131.75.176/api/notifications", parameters: params).responseJSON { (_, _, jsonObject, _) in
//				let json = SwiftyJSON.JSON(jsonObject!)
//				if let recepients = json["value"].array {
//				}
//		}
		Alamofire.request(Method.GET, "http://104.131.75.176/api/notifications", parameters: params)
			.responseJSON { (request, response, jsonObject, error) in
				if let jsonObject: AnyObject = jsonObject {
					let json = SwiftyJSON.JSON(jsonObject)
					if let recepients = json.array {
						self.eventsData = recepients
						self.collectionView.reloadData()
					}
				}
		}

		
//		Alamofire.request(.GET, "http://donorua-api.azurewebsites.net/version/1/recipients", encoding: .JSON)
//		.responseJSON { (_, _, jsonObject, _) in
//			let json = SwiftyJSON.JSON(jsonObject!)
//
//			if let recepients = json["value"].array {
//				self.eventsData = recepients
//				self.collectionView.reloadData()
//			}
//		}
	}
	
	func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		return 1
	}
	
	// cell count
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//		return filtersMenuItems.count + (isLoading ? 1 : 0) // For loading cell
		return eventsData.count
	}
	
	// cell configuration
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
//		if indexPath.row < filtersMenuItems.count {
			let cell = collectionView.dequeueReusableCellWithReuseIdentifier("EventCell", forIndexPath: indexPath) as! EventCell
			let event = eventsData[indexPath.row]
			let url = NSURL(string: event["image"].string!)
			cell.bgImageView.hnk_setImageFromURL(url!)
			cell.textLabel.text = event["title"].string! //+ event["lastName"].string!
			return cell
//		} else {
//			let cell = collectionView.dequeueReusableCellWithReuseIdentifier("HPVodLoadingCell", forIndexPath: indexPath) as! HPVodLoadingCell
//			return cell
//		}
	}
	
	func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
//		if indexPath.row + 1 == filtersMenuItems.count && !isLoading {
//			loadNextMenuItems()
//		}
	}
	
	// cell selection
	func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		var detailsViewController = storyboard.instantiateViewControllerWithIdentifier("EventDetailsViewController") as! EventDetailsViewController
		detailsViewController.data = [eventsData[indexPath.row]]
		navigationController?.pushViewController(detailsViewController, animated: true)
	}
	
	// cell size
//	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
//		let screenSize: CGRect = UIScreen.mainScreen().bounds
//		let screenWidth = screenSize.width
//		var itemsPerRow: CGFloat = 2
//		let cellWidth: CGFloat = (screenWidth - (itemsPerRow * 16.0)) / itemsPerRow
//		return CGSize(width: cellWidth, height: cellWidth)
//	}
}

class EventCell: UICollectionViewCell {
	
	@IBOutlet weak var textLabel: UILabel!
	@IBOutlet weak var bgImageView: UIImageView!
}
